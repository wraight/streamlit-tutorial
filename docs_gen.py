import os
from datetime import datetime
import shutil

#####################
### useful functions
#####################

def GetDateTimeNotice():
    dtArr=["!!! notice",
          f"\tPage generated: {datetime.now().date()} ({datetime.now().time()})",
          "\tFor updates/fixes contact: wraightATcern.ch"
        ]
    ### convert to string
    dtStr=""
    for ia in dtArr:
        dtStr+=f"{ia}\n\n"
    
    return dtStr


#####################
### main function
#####################
def main():

    ### find insteresting directories
    print("Copying READMEs")
    readmeFiles=[]
    for d in sorted(os.listdir(os.getcwd())):
        if os.path.isfile(d+"/README.md"):
            print(f"\tfound directory: {d}")
            fileName=d.split('/')[-1]
            print(f"\twriting: {fileName}")
            shutil.copyfile(d+"/README.md", f"docs/{fileName}.md")
            readmeFiles.append(f"{fileName}")

    ### write index (with timestamp)
    print("Writing setup")
    shutil.copyfile(os.getcwd()+"/README.md", "docs/tutorial_setup.md")
    # timestamp index file
    with open("docs/index.md", "a") as content_file:
        content_file.write(GetDateTimeNotice())

    ### update yml
    # notebooks
    with open("mkdocs.yml", "a") as content_file:
        content_file.write("  - Tutorial:\n")
        content_file.write("    - setup: tutorial_setup.md\n")
        for rmf in readmeFiles:
            newStr=f"    - {' '.join([x for x in rmf.split('/')[-1].split('_')])}: {rmf+'.md'}\n"
            content_file.write(newStr)


### for commandline running
if __name__ == "__main__":
    main()
    print("All done.")
