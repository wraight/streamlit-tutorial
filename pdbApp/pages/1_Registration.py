import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX

import sys
import os

# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/pdbApp/scripts')
from commonCode import GetPDBAccess, SelectComponentType, SelectTestType, EditSchema


### introduction
st.title(':customs: Registration (component/testRun)')
st.write('### Select upload type and populate generic schema for upload')
st.write('useful packages')
st.write('- [pandas](https://pandas.pydata.org/docs/user_guide/index.html) for data wrangling')
st.write('- [itkdb](https://pypi.org/project/itkdb/) for PDB API')
st.write("---")

### input user info.
st.write("## Get PDB Token")
st.write("Input user credentials to get token")

if "client" in st.session_state.keys():
    st.write("__token already found__")
else:
    GetPDBAccess(st.session_state)

if "client" not in st.session_state.keys():
    st.write("Please register credentials")
    st.stop()

st.write("---")

st.write(f"## Select Upload")

modeList=['componentType', 'testType']
modeVal= st.radio("Select upload mode", modeList)

st.write("---")

st.write(f"## __{modeVal}__ Upload")

reqOpt=True
if st.checkbox("Use optional?"):
    st.write("Get non-required parameters.")
    reqOpt=False


if modeVal=="componentType":
    df_ret=SelectComponentType(st.session_state)

    # get generic schema if not defined...
    if "objSchema" not in st.session_state.keys():
        st.session_state['objSchema']=st.session_state['client'].get('generateComponentTypeDtoSample', json={'code':df_ret.iloc[0]['code'], 'project':df_ret.iloc[0]['project_code'], 'requiredOnly':reqOpt} )
    # ... or if something changes: st.session_state[KEY] to df_ret.iloc[0][KEY]
    keyMap={'componentType':"code", 'project':"project_code"}
    for k,v in keyMap.items():
        try:
            if st.session_state['objSchema'][k]!=df_ret.iloc[0][v]:
                st.session_state['objSchema']=st.session_state['client'].get('generateComponentTypeDtoSample', json={'code':df_ret.iloc[0]['code'], 'project':df_ret.iloc[0]['project_code'], 'requiredOnly':reqOpt} )
        except KeyError:
            st.session_state['objSchema']=st.session_state['client'].get('generateComponentTypeDtoSample', json={'code':df_ret.iloc[0]['code'], 'project':df_ret.iloc[0]['project_code'], 'requiredOnly':reqOpt} )

    st.write(f"Got schema for {df_ret.iloc[0]['name']} ({df_ret.iloc[0]['project_name']})")



elif modeVal=="testType":
    df_ret=SelectTestType(st.session_state)

    # get generic schema if not defined...
    if "objSchema" not in st.session_state.keys():
        st.session_state['objSchema']=st.session_state['client'].get('generateTestTypeDtoSample', json={'code':df_ret.iloc[0]['code'], 'componentType':df_ret.iloc[0]['componentType_code'], 'project':df_ret.iloc[0]['project_code'], 'requiredOnly':reqOpt} )
    # ... or if something changes: st.session_state[KEY] to df_ret.iloc[0][KEY]
    keyMap={'testType':"code", 'componentType':"componentType_code", 'project':"project_code"}
    for k,v in keyMap.items():
        try:
            if st.session_state['objSchema'][k]!=df_ret.iloc[0][v]:
                st.session_state['objSchema']=st.session_state['client'].get('generateTestTypeDtoSample', json={'code':df_ret.iloc[0]['code'], 'componentType':df_ret.iloc[0]['componentType_code'], 'project':df_ret.iloc[0]['project_code'], 'requiredOnly':reqOpt} )
        except KeyError:
            st.session_state['objSchema']=st.session_state['client'].get('generateTestTypeDtoSample', json={'code':df_ret.iloc[0]['code'], 'componentType':df_ret.iloc[0]['componentType_code'], 'project':df_ret.iloc[0]['project_code'], 'requiredOnly':reqOpt} )

    st.write(f"Got schema for {df_ret.iloc[0]['name']} ( {df_ret.iloc[0]['componentType_name']} ({df_ret.iloc[0]['project_code']}) )")

else:
    st.write("Mode __{modeVal}__ not understood")
    st.stop()

st.write("---")

if "objSchema" not in st.session_state.keys():
    st.write("Please select schema")
    st.stop()

st.write("## Review Upload")

EditSchema(st.session_state)

st.write("---")

### upload schema to PDB
st.write("## Upload to PDB")

if st.button("Upload schema!"):
    try:
        if modeVal=="componentType":
            st.session_state['upVal']=st.session_state['client'].post('registerComponent', json=st.session_state['objSchema'])
            try:
                st.write(f"### **Successful {modeVal} registration **: {st.session_state['upVal']['component']['serialNumber']}")
            except KeyError:
                st.write(f"### **Successful {modeVal} registration **: {st.session_state['upVal']} (no component serialNumber)")
        if modeVal=="testType":
            st.session_state['upVal']=st.session_state['client'].post('uploadTestRunResults', json=st.session_state['objSchema'])
            try:
                st.write(f"### **Successful {modeVal} registration **: {st.session_state['upVal']['testRun']['id']}")
            except KeyError:
                st.write(f"### **Successful {modeVal} registration **: {st.session_state['upVal']} (no testRun id)")
        st.balloons()
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except TypeError:
        st.write("Don't have return value :(")

### offer delete if upload exists
if "upVal" in st.session_state.keys():
    if "component" in st.session_state['upVal'].keys():
        st.write(f"Previous component upload: {st.session_state['upVal']['component']['code']}")
    if "testRun" in st.session_state['upVal'].keys():
        st.write(f"Previous testRun upload: {st.session_state['upVal']['testRun']['id']}")
    # check upload
    if st.checkbox("See upload"):
        st.write(st.session_state['upVal'])
    # delete
    if st.button("Delete component"):
        try:
            if "component" in st.session_state['upVal'].keys():
                delVal=st.session_state['client'].post('deleteComponent', json={'component':st.session_state['upVal']['component']['code']})
                st.write("### **Successful component deletion **:",delVal['component']['code'])
            if "testRun" in st.session_state['upVal'].keys():
                delVal=st.session_state['client'].post('deleteTestrun', json={'testRun':st.session_state['upVal']['testRun']['id']})
                st.write("### **Successful testRun deletion **:",delVal['testRun']['id'])
            st.balloons()
        except itkX.BadRequest as b:
            st.write("### :no_entry_sign: **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        except TypeError:
            st.write("Don't have return value :(")
    
