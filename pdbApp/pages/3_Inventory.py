import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX

import pandas as pd
import altair as alt
import json

import sys
import os

# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/pdbApp/scripts')
from commonCode import GetPDBAccess


### introduction
st.title(':customs: Institution Inventory')
st.write('### Add some information from the PDB')
st.write('useful packages')
st.write('- [pandas](https://pandas.pydata.org/docs/user_guide/index.html) for data wrangling')
st.write('- [itkdb](https://pypi.org/project/itkdb/) for PDB API')
st.write("---")

### input user info.
st.write("## Get PDB Token")
st.write("Input user credentials to get token")

if "client" in st.session_state.keys():
    st.write("__token already found__")
else:
    GetPDBAccess(st.session_state)

if "client" not in st.session_state.keys():
    st.write("Please register credentials")
    st.stop()

st.write("---")

if "client_user" not in st.session_state.keys() or st.button("reset user"):
    st.session_state['client_user']=st.session_state['client'].get('getUser', json={'userIdentity': st.session_state['client'].user.identity})
    st.info(f"Returning token for {st.session_state['client_user']['firstName']} {st.session_state['client_user']['lastName']}")

if st.checkbox("See full user info."):
    st.write(st.session_state['client_user'])

## use first institution by default
instIdx=0
# userful shorthand
userInst=st.session_state['client_user']['institutions'][instIdx]['code']
userProj=st.session_state['client_user']['preferences']['defaultProject']

st.write(f"## Inventory Summary for {userInst} ({userProj})")

if "compList" not in st.session_state.keys() or st.button("reset component list"):
    st.session_state['compList']=st.session_state['client'].get('listComponents', json={'institution': userInst, 'project': userProj})

### check components found
if len(st.session_state['compList'].data)<1:
    st.write("No components found :(")
    st.stop()

df_compList=pd.json_normalize(st.session_state['compList'].data, sep = "_")

df_comps=df_compList.sort_values(by=['componentType_code','type_code','currentStage_code']).reset_index(drop=True)

df_comps_gr=df_compList.groupby(by=['componentType_code','type_code']).count().reset_index()
st.write(df_comps_gr[['componentType_code','type_code','id']].rename(columns={'id':"count"}))

countChart=alt.Chart(df_comps).mark_bar().encode(
    y='componentType_code:N',
    x='count():Q',
    color='type_code:N',
    tooltip=['componentType_code:N','count():Q','type_code:N']
).properties(title="Total Summary", width=800)
st.write(countChart)

st.write("## Component Type info.")
compTypeSel=st.selectbox("Select componentType", df_comps['componentType_code'].unique())

df_sel=df_comps.query(f'componentType_code=="{compTypeSel}"')

st.write(f"### Simple {compTypeSel} Stats")
for compState in ["ready","reworked","trashed"]:
    st.write("  -",compState,":",df_sel.query(f'state=="{compState}"')['serialNumber'].count())

# st.write(df_sel.columns)

stageChart=alt.Chart(df_sel).mark_bar().encode(
    y='currentStage_code:N',
    x='count():Q',
    color='type_code:N',
    tooltip=['currentStage_code:N','count():Q','type_code:N']
).properties(title=f"{compTypeSel} Summary", width=800)
st.write(stageChart)


st.write("## Component info.")
idSel=st.radio('identifier:',['serialNumber','alternativeIdentifier','code'])

compSel=st.selectbox(f"Select compontent ({idSel})", df_sel[idSel].unique())

st.write(f"### {st.session_state['compInfo'][idSel]} info.")
# st.write(df_sel.query(f'{idSel}=="{compSel}"').reset_index(drop=True).loc[0])

if "compInfo" not in st.session_state.keys() or st.button("reset component") or st.session_state['compInfo'][idSel]!=compSel:
    st.session_state['compInfo']=st.session_state['client'].get('getComponent', json={'component': compSel})

if st.checkbox('See full component info.'):
    st.write(st.session_state['compInfo'])

st.write("### Relatives")
for rel in ['parents','children']:
    st.write(f" - __{rel}__")
    try:
        # st.dataframe(st.session_state['compInfo'][rel])
        if st.session_state['compInfo'][rel]!=None:
            df_rel=pd.json_normalize(st.session_state['compInfo'][rel], sep = "_")
            st.write(df_rel)
        else:
            st.write(f"No {rel} found")
    except KeyError:
        st.write(f"No {rel} key found")
    except TypeError:
        st.write(f"No {rel} object found")


for rel in ['properties','tests']:
    st.write(f"### {rel.title()}")
    try:
        # st.dataframe(st.session_state['compInfo'][rel])
        if st.session_state['compInfo'][rel]!=None:
            df_rel=pd.json_normalize(st.session_state['compInfo'][rel], sep = "_")
            st.write(df_rel)
        else:
            st.write(f"No {rel} found")
    except KeyError:
        st.write(f"No {rel} key found")
    except TypeError:
        st.write(f"No {rel} object found")
