import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX

import pandas as pd
import altair as alt
import json

import sys
import os

# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/pdbApp/scripts')
from commonCode import GetPDBAccess, SelectComponentType, GetPopulation


### introduction
st.title(':customs: Basic Reporting')
st.write('### Add some information from the PDB')
st.write('useful packages')
st.write('- [pandas](https://pandas.pydata.org/docs/user_guide/index.html) for data wrangling')
st.write('- [itkdb](https://pypi.org/project/itkdb/) for PDB API')
st.write("---")

### input user info.
st.write("## Get PDB Token")
st.write("Input user credentials to get token")

if "client" in st.session_state.keys():
    st.write("__token already found__")
else:
    GetPDBAccess(st.session_state)

if "client" not in st.session_state.keys():
    st.write("Please register credentials")
    st.stop()

st.write("---")

st.write("## Define Population")

st.write("Select project and componentType")

df_ret=SelectComponentType(st.session_state)

# get all componentTypes then filter
# if "compTypeList" not in st.session_state.keys():
#     st.session_state['compTypeList']=st.session_state['client'].get('listComponentTypes', json={'project':st.session_state['project'][0], 'componentType':"MODULES" }).data

compObj=st.session_state['client'].get('getComponentTypeByCode', json={'code':df_ret.iloc[0]['code'], 'project':df_ret.iloc[0]['project_code'] } )

if st.checkbox("See full componentType object?"):
    st.write(compObj)

infoList=[k for k,v in compObj.items() if type(v)==type([])]

infoVal=st.selectbox("Select componentType info.",  infoList)
st.dataframe(compObj[infoVal])


st.write("---")

st.write("## Select Property")

try:
    selProp=st.selectbox("Select componentType property",[x['code'] for x in compObj['properties']])
except:
    st.error(f"componentType: {df_ret.iloc[0]['code']} ({df_ret.iloc[0]['project_code']}) has no properties :(")
    st.stop()

if "popList" not in st.session_state.keys() or st.session_state['popPara']!=selProp or 1==1:
    if st.button("Get Population"):
        st.session_state['popPara']=selProp
        st.session_state['popList']=GetPopulation(st.session_state['client'],df_ret.iloc[0]['project_code'],df_ret.iloc[0]['code'],selProp)

if "popList" not in st.session_state.keys():
    st.write("Retrieve population from PDB")
    st.stop()

if st.checkbox('See population list'):
    st.dataframe(st.session_state['popList'])

st.write("---")

st.write("## Visualisation")

st.write(f"{st.session_state['popPara']} information from {df_ret.iloc[0]['code']} ({df_ret.iloc[0]['project_code']})")


### dataframe
df_prop=pd.DataFrame(st.session_state['popList'])
df_prop['prop_ed']=df_prop['property']
noZeros=False
if st.checkbox("Remove zeros?"):
    try:
        st.write(" - removing zeros")
        df_prop['prop_ed']=df_prop['property'].apply(lambda x: None if x==0 else x)
        noZeros=True
    except:
        st.write("Issue removing zeros. No action taken. Check data.")
        
# st.write(df_prop)

statsList=[]
text=st.empty()
for t in df_prop['type'].unique():
    text.write(f"working on {t}")
    df_type=df_prop.query('type=="'+t+'"')
    statsList.append({'type':t, 'all':df_type['property'].count()})
    # if property is numeric
    try:
        statsList[-1]['max']=df_type['prop_ed'].max()
        statsList[-1]['min']=df_type['prop_ed'].min()
        statsList[-1]['mean']=df_type['prop_ed'].mean()
    except TypeError:
        pass
    if noZeros==True:
        statsList[-1]['noZero']=df_type['prop_ed'].count()
else:
    text.write("done")

df_stats=pd.DataFrame(statsList)
st.write(df_stats)

st.write("Showing the data")

### values
chart=alt.Chart(df_prop).mark_bar().encode(
        x=alt.X('prop_ed:Q', bin=False, title=st.session_state['popPara']),
        y=alt.Y('count():Q'),
        color=alt.Color('type:N'),
        tooltip=['count():Q','type:N']
        ).properties(title=st.session_state['popPara']+" histogram (auto-binning)", width=600).interactive()
st.write(chart)

### measured
chart_prop=alt.Chart(df_prop).mark_boxplot(size=50, extent=0.5).encode(
    x=alt.X("type:N", axis=alt.Axis(labelAngle=-45) ),
    y=alt.Y("prop_ed:Q",scale=alt.Scale(zero=False), title=st.session_state['popPara'] ),
    color=alt.Color("type:N", legend=None)
).properties(title=st.session_state['popPara']+" by type", width=600)
st.write(chart_prop)