import streamlit as st
import streamlit.components.v1 as components

import random
import pandas as pd
import altair as alt
import plotly.express as px
from bokeh.plotting import figure, show
# select a palette
from bokeh.palettes import Category10
# itertools handles the cycling
import itertools 

### introduction
st.title(':chart_with_upwards_trend: Plotting')
st.write('### Make a simple plot')
st.write('useful packages')
st.write('- [pandas](https://pandas.pydata.org/docs/user_guide/index.html) for data wrangling')
st.write('- [altair](https://altair-viz.github.io/user_guide/generated/toplevel/altair.Chart.html) for plotting')
st.write('- [plotly express](https://plotly.com/python/plotly-express/) for plotting')

st.write("---")

### settings
st.write("## Make data")
st.write("Use some widgets to help user select options")

st.write("### Sample Settings")
sampleSize=st.slider("Set sample size", min_value=1, max_value=50, value=20, step=1)
minVal, maxVal = st.select_slider("Select range of values", options=range(0,100,5), value=[0,20])

checkVal=st.checkbox("Set threhold (used for plot colour)", value=False)
if checkVal:
    threshold=st.slider("Threshold value", min_value=int(minVal), max_value=int(maxVal), value=int((maxVal-minVal)/2), step=1)
else:
    threshold=maxVal/2


st.write("---")

### pandas
st.write("## Wrangle data")
st.write("Using _pandas_ package")

dataList=[]
for i in range(0,sampleSize+1,1):
    dataList.append({'x':i,'y':random.randint(minVal, maxVal)})
    if dataList[-1]['y']<threshold:
        dataList[-1]['other']="below"
    else:
        dataList[-1]['other']="above"
st.write(f"got data! size: {len(dataList)}")

st.write("### Visualise data")
df_data=pd.DataFrame(dataList)
st.write("using dataframe:", df_data)

st.write("---")

### plotting
st.write("## Plotting")
st.write("Several plotting packages supported: altair, plotly, bokeh ...")

st.write("## 1. _altair_ example")
dataChart=alt.Chart(df_data).mark_circle(size=60).encode(
                x=alt.X('x:Q',title="x values"),
                y=alt.X('y:Q',title="y values"),
                color=alt.Color('other:N',title="threshold pass?"),
                tooltip=['x:Q', 'y:Q', 'other:N']
            ).properties(
                title="Values passing threshold: "+str(threshold),
                width=600
            ).interactive()
st.write(dataChart)

st.write("## 2. _plotly_ example")
fig = px.scatter(df_data, x="x", y="y", color="other", title="Values passing threshold: "+str(threshold))
st.write(fig)


st.write("## 3. _bokeh_ example")

# create a color iterator
def get_colour():
    yield from itertools.cycle(Category10[10])
colour = get_colour()

p = figure(title="Values passing threshold: "+str(threshold), x_axis_label='x', y_axis_label='y')
for o, c in zip(df_data['other'].unique(), colour):
    p.circle(df_data.query('other=="'+o+'"')['x'].to_list(), df_data.query('other=="'+o+'"')['y'].to_list(), color=c, legend_label=o, line_width=2)

st.write(p)
