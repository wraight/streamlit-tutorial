# Simple webApp example

Demonstrate basic streamlit functionality

Contents:

 * [Text](#text)
 * [Plotting](#plotting)
 * [Scripting](#scripting)
 * [Widgets](#widgets)
 * [Caching](#caching)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Running example code

From repository top directory:

 - __local running__
 > streamlit run simpleApp/simpleApp.py --server.port=8501

 - __docker running__

    - build image
    > docker build -f dockerFiles/Dockerfile_simpleApp

    - run with local directories mounted
    > docker run -p 8501:8501 -v $(pwd)/simpleApp/:/code/simpleApp/ simple-app

Open app in browser with localhost:8501

## Simple

This page covers some simple functionality to display information in in streamlit

### Text

Text in Streamlit is added using [st.write](https://docs.streamlit.io/library/api-reference/write-magic/st.write) function. 

This is quite flexible (_"Swiss Army knife of Streamlit commands"_) and can be used display plain text, markdown formatted text ([cheatsheet](https://www.markdownguide.org/cheat-sheet/)) and objects such as jsons, [pandas](https://pandas.pydata.org) dataframes and images.


Some examples of some text:

 - plain text: 
``` python
st.write("Warriors come out to play.")
```

 - markdown text: 
``` python
st.write("**Warriors** come out to _play_.")
```

 - coloured text boxes
``` python
st.success("Warriors are playing.")
st.info("The Warriors is a 1979 American action thriller film directed by Walter Hill.")
st.error("No Warriors are available at this time.")
```

 - json: 
``` python
st.write("My json/df/img:", my_obj)
```

A specific function is available for images: [st.image](https://docs.streamlit.io/library/api-reference/media/st.image):
``` python
st.image("PATH_TO_FILE", caption="optional caption") 
```

## Plotting

Streamlit works well with many visualisation packages including [altair](https://altair-viz.github.io), [plotly](https://plotly.com) and [bokeh](https://bokeh.org).

 - the preferred plotting package is somewhat a matter of taste. In the following tutorial apps _altair_ is used as it easily interfaces with _pandas dataframes_ and has a wide range of functionality and documented examples. 

The [st.write](https://docs.streamlit.io/library/api-reference/write-magic/st.write) function can be used to include the plots.

``` python
st.write("My latest plot:", plot_obj) 
```

## Scripting

Since streamlit is written in python it is easy to include scripts.

Simply add them to the code as appropriate just like any script:

 - define functions in the same file
 - import functions from other files
    - particularly for factoring out large chunks of code or commmonly repeated code

``` python
### import function from file
from my_file import some_script 
### execute on button click
if st.button("Do it!"):
    some_import.some_script()
```

## Widgets

One of the main uses of web applications is as an interface for user input. Streamlit has a range of [widgets](https://docs.streamlit.io/library/api-reference/widgets) for this, from raw text to sliders, from calanders to file uploads.

File upload example:

``` python
upFile = st.file_uploader('Upload JSON file', type="json")
```

__NB__ In order for streamlit to keep widget interactions in order each widget must be unique. This is done simply using widget arguments, e.g. for [st.button](https://docs.streamlit.io/library/api-reference/widgets/st.button) object the text label is enough.

For more complicated cases, e.g. iteractions over arrays, widgets can be explicitly distinguished using the _key_ attribute.

For example:

``` python
### looping buttons
for e,x in enumerate(['this','that','other']):
    if st.button("Do it!", key=e):
        st.write(f"text: {x}")
```

## Caching

Streamlit runs code as a script from top to bottom. Each user event, i.e. widget interaction, re-runs the script. Naively this can lead to issues with caching:

For example:

``` python
### execute on button click
if st.button("Button A"):
    st.write("You clicked A!")

if st.button("Button B"):
    st.write("You clicked B!")
```
 - here both pieces of text will not be shown at the same time.

The [st.session_state](https://docs.streamlit.io/library/api-reference/session-state) object is used to cache user inputs. As you might expect the cached information is held until the app closes or is reset.

For example:

``` python
### execute on button click
if st.button("Button A"):
    st.session_state['A']=True
if "A" in st.session_state.keys() and st.session_state['A']==True:
    st.write("You clicked A!")

if st.button("Button B"):
    st.session_state['b']=True
if "B" in st.session_state.keys() and st.session_state['B']==True:
    st.write("You clicked B!")
```
 - now both pieces of text will be shown at the same time (refreshing the page will clear the text).


## Streamlit Error Message (_aka_ The Big Red Box)

Error messages from Streamlit are presented in a __Big Red Box__. These usually include the error type in the title, perhaps a reference to a line and the full python output - which provides the traditional trail of breadcomes to issue.

## Browser functionality

Streamlit has a couple of features to aid development which can be found using the [hamburger button](https://en.wikipedia.org/wiki/Hamburger_button) at the top right hand side of the browser window:

- Rerun:

    - this re-executes the code __and__ retains session state information. In contrast, refreshing the page will lose any session state information.
    This helps development where you want to hotfix some code and rerun without inputting all information again.

- Record a screencast:

    - "This will record a video with the contents of your screen, so you can easily share what you're seeing with others."

        - audio is optional
        - optional area: tab, window or entire screen
            - pop-ups (e.g. file browser) are not recorded using the "tab" setting
        - works on Chrome, Firefox, Edge
            - __NB__ not available with Safari
    
    - the output is a _webm_ file which can be shared and run through compatible browsers / applications.

