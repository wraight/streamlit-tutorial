# API webApp example

Examples of basic interaction with ITk PDB (via [itkdb](https://pypi.org/project/itkdb/) package)

Contents:

 * [Overview](#overview)
 * [Python itkdb Package](#python-itkdb-package)
 + [Accessing the ITk PDB](#accessing-the-itk-pdb)
 * [Downloading (GET) data from ITk PDB](#downloading--get--data-from-itk-pdb)
 * [Uploading (POST) data to ITk PDB](#uploading--post--data-to-itk-pdb)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Running example code

From repository top directory:

 - __local running__
 > streamlit run apiApp/apiApp.py --server.port=8501

 - __docker running__

    - build image
    > docker build -f dockerFiles/Dockerfile_apiApp -t api-app .

    - run with local directories mounted
    > docker run -p 8501:8501 -v $(pwd)/apiApp/:/code/apiApp/ api-app

Open app in browser with localhost:8501

## Overview

There are two basics elements to interacting with the ITk Production Database (PDB):

 - getting information, i.e. downloading data
 - posting information, i.e. uploading data

Scripted interaction with the ITk PDB is possible through an API. Common tools, such as python's requests package, can be used to access the ITk PDB, and the API [documentation](https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book) is provided.

 - access using ITk PDB access codes (or connected account if set-up, e.g. Google)
 - navigate to _uuCommands_ via the sidebar

![image info](images/sidebar.png){ width="200" }


## Python itkdb Package

Helpfully a python package for ITk PDB interaction has been developed to aid scripted interactions. The [itkdb](https://pypi.org/project/itkdb/) package is installable via the python package manager _pip_ and includes useful functionality to send and retrieve data from the ITk PDB (see documentaion for full details). 

The examples given here use the itkdb package.

### Accessing the ITk PDB

The first task is to get access to the ITk PDB. This is done using the two passwords you set when you registered with the PDB.

``` python
user=itkdb.core.User(
    access_code1=st.text_input("access code1:"),
    access_code2=st.text_input("access code2:")
    )
### on demand registration
if st.checkbox("Get token"):
    try:
        user.authenticate()
        client = itkdb.Client(user=user)
        st.success("Got PDB Token!")
    except itkdb.exceptions.ResponseException: # user itkdb exception
        st.error("Cannot authenticate user credentials")
```

The _client_ object can then be used to access the API.


## Downloading (GET) data from ITk PDB

The general form of ITk PDB GET queries via itkdb:
> client.get('COMMAND_STRING', json={...})

 - This will return a _json_ object listing matching objects, e.g. institutions, components, tests, etc.

A simple API command is [_listProjects_](https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book/page?code=56313594) (no input json is required):

``` python
client.get('listProjects', json={})
```

A more complicated command to get componentType information from the database is [_getComponentTypeByCode_](https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book/page?code=31391419):
 - the required inputs to uniquely identify the componentType are the _code_ of the componentType and the _project_ code

For example (strips modules): 

``` python
client.get('getComponentTypeByCode', json={'code':"MODULE",'project':"S"})
```

## Uploading (POST) data to ITk PDB

The general form of ITk PDB POST queries via itkdb:
> client.post('COMMAND_STRING', json={...})

 - This will return a _json_ object of the created object, e.g. component, test, institution, etc.
 - __NB__ deleting objects from the database is also done via posting

Example upload:

``` python
if st.button("Upload to PDB!"):
    try:
        regVal=client.post('registerComponent', json=reg_obj)
        try:
            st.write("### **Successful component registration **:",regVal['component']['serialNumber'])
        except KeyError:
            st.write("### **Successful component registration **:",regVal, "(no component serialNumber)")
        st.balloons()
    except itkdb.exceptions.BadRequest as b:
        st.write("### :no_entry_sign: **Unsuccessful**")
        ### get error message sub-string
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except TypeError:
        st.write("Don't have return value :(")
```

