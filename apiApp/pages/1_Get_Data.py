import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX

import pandas as pd
import json


### introduction
st.title(':customs: Example ITk Production Database interaction - GET')
st.write('### Retrieve some information from the PDB')
st.write('useful packages')
st.write('- [pandas](https://pandas.pydata.org/docs/user_guide/index.html) for data wrangling')
st.write('- [itkdb](https://pypi.org/project/itkdb/) for PDB API')
st.write("---")

### input user info.
st.write("## Get PDB Token")
st.write("Input user credentials to get token")

if "client" in st.session_state.keys():
    st.write("__token already found__")
else:
    st.write("### Input Credentials")
    # cache credentials in dictionary
    if "userTokens" not in st.session_state:
        st.session_state['userTokens']={'ac1':None,'ac2':None}

    radSel=st.radio("Input selection:",["file","keyboard"])

    if radSel=="keyboard":
        st.write("Please input tokens via keyboard")
        st.session_state['userTokens']['ac1']=st.text_input("first access token", type="password")
        st.session_state['userTokens']['ac2']=st.text_input("second access token", type="password")
    else:
        st.write("Please input tokens via file")
        st.json({'ac1':"YOUR_AC1",'ac2':"YOUR_AC2"})
        ## drag and drop method
        upFile = st.file_uploader('Upload JSON file', type="json")
        if upFile is not None:
            if st.checkbox("Check upload?"):
                st.write("uploaded file:",upFile.getvalue().decode('utf-8'))
            credDict = json.load(upFile)
            # format keys (use same dictionary)
            for ac in ["AC1","AC2"]:
                if ac.lower() in [k.lower() for k in credDict.keys()]:
                    try:
                        st.session_state['userTokens'][ac.lower()]=credDict[ac.lower()]
                    except KeyError:
                        st.session_state['userTokens'][ac.lower()]=credDict[ac]
                    except:
                        st.write(f"Cannot find {ac} in file")
                else:
                    st.write("No file uploaded")
                    st.download_button(label="Download example JSON", data=json.dumps({'ac1':"YOUR_AC1",'ac2':"YOUR_AC2"}, indent=2), file_name="credentials.json")

    ### ready to get token
    if st.session_state['userTokens']['ac1']!=None and st.session_state['userTokens']['ac2']!=None:
        st.write("Credentials found!")
        # debug option
        if st.checkbox("Check credentials?"):
            st.json(st.session_state['userTokens'])
        # get token
        if st.button("Get PDB token!"):
            st.write("use tokens to get client")
            st.session_state['user'] = itkdb.core.User(access_code1=st.session_state['userTokens']['ac1'], access_code2=st.session_state['userTokens']['ac2'])
            try:
                st.session_state['user'].authenticate()
                st.session_state['client'] = itkdb.Client(user=st.session_state['user'])
                st.success("Got PDB Token!")
            except itkX.ResponseException:
                st.error("Cannot authenticate user credentials")
                st.stop()
    else:
        st.stop()

if "client" not in st.session_state.keys():
    st.write("Please register credentials")
    st.stop()

st.write("---")

### Get some data
st.write("## Get ITk Institutions")
instList=st.session_state['client'].get('listInstitutions', json={} ).data

### using pandas dataframe to wrangle data
df_inst=pd.DataFrame(instList)
# filter names by "University" string
filtCheck=st.checkbox("Apply _University_ filter?")
if filtCheck:
    st.write("applying _University_ filter")
    df_inst=df_inst.query('name.str.contains("University")')
st.dataframe(df_inst)
st.write(f"No. of entries: {len(df_inst.index)}")

selInst=st.selectbox("Select institution:",options=df_inst['name'].to_list())

st.write(f"__{selInst}__ Information")
st.write( next(i for i in instList if i['name']==selInst) )
st.write("---")
