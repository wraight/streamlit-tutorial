import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX

import pandas as pd
import json


### introduction
st.title(':customs: Example ITk Production Database interaction - POST')
st.write('### Add some information from the PDB')
st.write('useful packages')
st.write('- [pandas](https://pandas.pydata.org/docs/user_guide/index.html) for data wrangling')
st.write('- [itkdb](https://pypi.org/project/itkdb/) for PDB API')
st.write("---")

### input user info.
st.write("## Get PDB Token")
st.write("Input user credentials to get token")

if "client" in st.session_state.keys():
    st.write("__token already found__")
else:
    st.write("### Input Credentials")
    # cache credentials in dictionary
    if "userTokens" not in st.session_state:
        st.session_state['userTokens']={'ac1':None,'ac2':None}

    radSel=st.radio("Input selection:",["file","keyboard"])

    if radSel=="keyboard":
        st.write("Please input tokens via keyboard")
        st.session_state['userTokens']['ac1']=st.text_input("first access token", type="password")
        st.session_state['userTokens']['ac2']=st.text_input("second access token", type="password")
    else:
        st.write("Please input tokens via file")
        st.json({'ac1':"YOUR_AC1",'ac2':"YOUR_AC2"})
        ## drag and drop method
        upFile = st.file_uploader('Upload JSON file', type="json")
        if upFile is not None:
            if st.checkbox("Check upload?"):
                st.write("uploaded file:",upFile.getvalue().decode('utf-8'))
            credDict = json.load(upFile)
            # format keys (use same dictionary)
            for ac in ["AC1","AC2"]:
                if ac.lower() in [k.lower() for k in credDict.keys()]:
                    try:
                        st.session_state['userTokens'][ac.lower()]=credDict[ac.lower()]
                    except KeyError:
                        st.session_state['userTokens'][ac.lower()]=credDict[ac]
                    except:
                        st.write(f"Cannot find {ac} in file")
                else:
                    st.write("No file uploaded")
                    st.download_button(label="Download example JSON", data=json.dumps({'ac1':"YOUR_AC1",'ac2':"YOUR_AC2"}, indent=2), file_name="credentials.json")

    ### ready to get token
    if st.session_state['userTokens']['ac1']!=None and st.session_state['userTokens']['ac2']!=None:
        st.write("Credentials found!")
        # debug option
        if st.checkbox("Check credentials?"):
            st.json(st.session_state['userTokens'])
        # get token
        if st.button("Get PDB token!"):
            st.write("use tokens to get client")
            st.session_state['user'] = itkdb.core.User(access_code1=st.session_state['userTokens']['ac1'], access_code2=st.session_state['userTokens']['ac2'])
            try:
                st.session_state['user'].authenticate()
                st.session_state['client'] = itkdb.Client(user=st.session_state['user'])
                st.success("Got PDB Token!")
            except itkX.ResponseException:
                st.error("Cannot authenticate user credentials")
                st.stop()
    else:
        st.stop()

if "client" not in st.session_state.keys():
    st.write("Please register credentials")
    st.stop()

st.write("---")

if "compTypeCode" not in st.session_state.keys():
    st.session_state['compTypeCode']="MODULE"

if st.checkbox(f"Change from {st.session_state['compTypeCode']}?"):
    st.session_state['compTypeCode']=st.text_input("Enter componentType code:")

### Get some data
st.write(f"## Get {st.session_state['compTypeCode']} ComponentType Data")

st.write("Select project and check componentType information.")

st.session_state['project']=st.radio("Select Project:",["Strips", "Pixels"])

st.write("Project selected:", st.session_state['project'])

# get all componentTypes then filter
# if "compTypeList" not in st.session_state.keys():
#     st.session_state['compTypeList']=st.session_state['client'].get('listComponentTypes', json={'project':st.session_state['project'][0], 'componentType':"MODULES" }).data

compObj=st.session_state['client'].get('getComponentTypeByCode', json={'project':st.session_state['project'][0], 'code':st.session_state['compTypeCode'] } )

infoList=["subprojects", "types", "stages", "properties"]

infoVal=st.selectbox("Select component info.",  infoList)
if st.checkbox("See full object info."):
    st.write(compObj[infoVal])
df_info=pd.DataFrame(compObj[infoVal])
for col in df_info.columns:
    df_info[col]=df_info[col].apply(lambda x: [y['code'] for y in x if type(y)==type({}) and "code" in y.keys()] if type(x)==type([]) else x)
    df_info[col]=df_info[col].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
st.write(df_info)

st.write("---")

### retrieve generic schema
st.write("### Get Generic Schema")

st.write("Fill generic json with appropriate (see above) information")

reqOpt=True
if st.checkbox("Use optional?"):
    st.write(" - Get non-required parameters.")
    reqOpt=False
# st.write({'code':df_ct.iloc[0]['code'], 'project':df_ct.iloc[0]['project_code']})
# get generic schema
if "objSchema" not in st.session_state.keys() or st.session_state['objSchema']['project']!=st.session_state['project'][0]:
    st.session_state['objSchema']=st.session_state['client'].get('generateComponentTypeDtoSample', json={'code':st.session_state['compTypeCode'], 'project':st.session_state['project'][0], 'requiredOnly':reqOpt} )

st.write(f"Got schema for {st.session_state['objSchema']['componentType']} ({st.session_state['objSchema']['project']})")

### try to match stuff
def SelectCheck(k,v, inJson, inKey=None):
    try:
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v), key=inKey)
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, type='default', key=inKey)
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError: # non string types will not have lower() attribute
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v), key=inKey)
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, type='default', key=inKey)
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    # inVal=stTrx.Tryeval(val)
    # inVal=stTrx.MatchType(v,inVal)
    return val #inVal

if st.checkbox("Edit Schema?"):
    
    for k,v in st.session_state['objSchema'].items():
        if type(v)==type({}):
            st.write("**"+str(k)+"**")
            for l,w in v.items():
                # inJson[k][l]=SelectCheck(l,w)
                st.session_state['objSchema'][k][l]=SelectCheck(l,w,st.session_state['objSchema'])
        else:
            # inJson[k]=SelectCheck(k,v)
            st.session_state['objSchema'][k]=SelectCheck(k,v,st.session_state['objSchema'])

else:
    st.write(st.session_state['objSchema'])

st.write("---")


### upload schema to PDB
st.write("## Upload to PDB")

if st.button("Upload schema!"):
    try:
        st.session_state['upVal']=st.session_state['client'].post('registerComponent', json=st.session_state['objSchema'])
        try:
            st.write("### **Successful component registration **:",st.session_state['upVal']['component']['serialNumber'])
        except KeyError:
            st.write("### **Successful component registration **:",st.session_state['upVal'], "(no component serialNumber)")
        st.balloons()
    except itkX.BadRequest as b:
        st.write("### :no_entry_sign: **Unsuccessful**")
        st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
    except TypeError:
        st.write("Don't have return value :(")

### offer delete if upload exists
if "upVal" in st.session_state.keys():
    st.write(f"Previous upload: {st.session_state['upVal']['component']['code']}")
    # check upload
    if st.checkbox("See upload"):
        st.write(st.session_state['upVal'])
    # delete
    if st.button("Delete component"):
        try:
            delVal=st.session_state['client'].post('deleteComponent', json={'component':st.session_state['upVal']['component']['code']})
            st.write("### **Successful component deletion **:",st.session_state['upVal']['component']['code'])
            st.balloons()
        except itkX.BadRequest as b:
            st.write("### :no_entry_sign: **Unsuccessful**")
            st.write(str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]) # sucks
        except TypeError:
            st.write("Don't have return value :(")
    