# Welcome to Streamlit Information for ITk

Some unofficial Streamlit information and tutorial
 - official Streamlit documentation lives [here](https://www.streamlit.io) 

## Code repository

Code lives [here](https://gitlab.cern.ch/wraight/streamlit-tutorial)


## Existing Streamlit webApps for ITk 

| Name      | Link | Description |
|-----------|-------------|:------|
| pixels    | [link](https://itk-pdb-webapps-pixels.web.cern.ch) | collections of pixels webApps |
| strips    | [link](https://itk-pdb-webapps-strips.web.cern.ch) | collection of strips webApps |
| common electronics | [link](https://itk-pdb-webapps-common-electronics.web.cern.ch) | collection of common electronics webApps |
| general   | [link](https://uk-itk-pdb-webapp-general.web.cern.ch) | infrequently updated testing app |
| reporting | [link](https://nihad-itk-qt-itk-reporting.app.cern.ch) | generate reports on demand |

## Some Credits

Documentation built using [squidfunk](https://squidfunk.github.io/mkdocs-material/)

 - additional inspitation from [itk-docs](https://itk.docs.cern.ch)

