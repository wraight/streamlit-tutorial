# Streamlit Tutorial

Tutorial on how to build [Streamlit](https://www.streamlit.io) based apps for ITk Production Database interface.
 - mkdocs pages are available [here](https://streamlit-for-itk.docs.cern.ch/)

ITk PDB access via [itkdb](https://pypi.org/project/itkdb/).

__NB__ See requirements file for essential packages.

### Contents

[Motivation](#motivation)
 - some propoganda

[Pre-requisites](#pre-requisites)
 - things to have before the tutorial

[Running locally](#running-locally)
 -  using streamlit on local system

[Running with Docker](#running-with-docker) (recommended)
 - using docker to run apps via containers

[Hosting at Cern](#hosting-at-cern)
 - host docker containers at Cern with OpenShift platform

App List:

 - _simpleApp_: demonstrate basic streamlit functionality
    - includes: plotting, using scripts and caching
 - _apiApp_: examples of basic interaction with ITk PDB (via itkdb package)
    - includes: downloading (GET) and uploading (POST) data to ITk PDB
 - _pdbApp_: examples of useful webApp functionality
    - includes: generating schemas for data upload and basic reporting


## Motivation

Basically, the ITk Production Database (PDB) holds a lot of information as the as the common book-keeping resource for the Inner Tracker Upgrade.

The [Unicorn web GUI](https://itkpd-test.unicorncollege.cz/testTypes) provides comprehensive functionality in interfacing with the PDB, _but_ it is __not optimised__ for specific interactions (e.g. test uploads with analysis or reporting). Custom interfaces ease PDB interaction and _Web Applications_ are interfaces built on simple platforms using PDB API tools.

Other interfaces are available:

 - tkinter:  Python's de-facto standard GUI
    - this can be difficult to run across platforms
    - Streamlit uses the native browser to host the page

 - flask: provides comporehensive control of web app layout
    - can take a while to customise
    - Streamlit is intended for _fast_ development

 - dash: alternative webapp development framework
    - different code layout
    - Streamlit is run _top-to-bottom_, like a script

Some introductory [slides](https://indico.cern.ch/event/1140037/contributions/4783975/attachments/2417873/4137976/WP12_22_3_30_F2F.pdf) on Streamlit for PDB interfaces.

## Clone the repository

You can see the full git repository from the link [here](https://gitlab.cern.ch/wraight/streamlit-tutorial) (and linked at the top right hand side of the screen).

To clone the repository:

> git clone https://gitlab.cern.ch/wraight/streamlit-tutorial

Navigate into the _streamlit-tutorial_ directory and begin the tutorial.

### Get the pre-requisites

This tutorial is python(3) based.

You can find essential python packages in the _requirements_ file and install via python's _pip_ module:
> python -m pip install -r requirements.txt

Docker development is recommended. A useful (linux/mac/Windows) desktop application is available [here](https://www.docker.com/products/docker-desktop/).


## Running locally

Run _streamlit_ locally in the top directory of the repository. Specify the _App_ file in a directory. Pages will be picked up form _pages_ sub-directory.

### Install requirements

Make sure to install the required packages.

E.g. installing with _pip_:
> python -m pip install -r requirements.txt

### Run streamlit

E.g. to run the _simpleApp_:

> streamlit run simpleApp/simpleApp.py --server.port=8501

arguments:
- set port (optional): --server.port (8501 default)

### Access app

To access the app open a browser with address "localhost:[port]"
 - by default: localhost:8501


## Running with Docker

This method requires extra set-up but will (hopefully) avoid issues with variations in platform, operating system, local package managers.

Running with Docker requires two steps:
 - image building
 - container running

Recommend downloading Docker desktop from [here](https://www.docker.com/products/docker-desktop/).

Run docker from the top directory of the reporsitory.

### Building the app image

DockerFiles are provided in the _dockerFiles_ directory.

E.g. to build the _simpleApp_ image:

> docker build -f dockerFiles/Dockerfile_simpleApp -t simple-app .

arguments:
- specify dockerfile path: -f
- specify image name: -t
- use current directory: .

### Running an app container (basic)

After the image has been built a container can be run.

E.g. to run a _simple-app_ container:

> docker run -p 8501:8501 simple-app

arguments:
- specify port mapping from local resource to inside container: -p (outside:inside)
- specify image name: final argument

### Running an app container with mounted directories (recommended)

Local directories can be mounted inside a container. This speeds up development as local changes can be implemented inside the container without the need to build the image.

E.g. to run a _simple-app_ container with the local directory mounted:

> docker run -p 8501:8501 -v $(pwd)/streamlit/pages/:/code/streamlit/pages/ simple-app

arguments:
- specify memory mapping from local resource to inside container: -v (outside:inside)

__NB__ for Windows users: $(pwd) --> $PWD

### Accessing app

Since the ports have been mapped, the app can be accessed via a browser with address "localhost:[port]"
 - the same address as running without docker: localhost:8501


## Hosting at Cern

See [reports repository](https://itk-reports.docs.cern.ch/cronJobs/).
